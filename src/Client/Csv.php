<?php

namespace Drupal\entity_sync_csv\Client;

use Drupal\entity_sync\Client\ClientInterface;

use OpenSpout\Common\Entity\Row;
use OpenSpout\Common\Helper\EncodingHelper;
use OpenSpout\Reader\CSV\Options;
use OpenSpout\Reader\CSV\Reader;
use OpenSpout\Reader\SheetInterface;

/**
 * Entity Synchronization client for importing/exporting to CSV files.
 */
class Csv implements ClientInterface {

  /**
   * {@inheritdoc}
   */
  public function importList(array $filters = [], array $options = []) {
    $reader = new Reader($this->prepareReaderOptions($options));
    $reader->open($options['file_path']);

    $objects = [];
    // Collect all rows from all sheets in the file. Since this is a CSV file,
    // there will always be one sheet.
    // phpcs:disable
    // @I Support a paging iterator instead of loading all objects at once
    //    type     : task
    //    priority : normal
    //    labels   : performance
    // phpcs:enable
    foreach ($reader->getSheetIterator() as $sheet) {
      $objects = array_merge($objects, $this->getSheetObjects($sheet));
    }

    $reader->close();

    return new \ArrayIterator($objects);
  }

  /**
   * {@inheritdoc}
   */
  public function importEntity($id) {
    throw new \Exception('Importing individual entities is not supported yet.');
  }

  /**
   * {@inheritdoc}
   */
  public function create(array $fields, array $options = []) {
    throw new \Exception('Exporting individual entities is not supported yet.');
  }

  /**
   * {@inheritdoc}
   */
  public function update($id, array $fields, array $options = []) {
    throw new \Exception('Exporting individual entities is not supported yet.');
  }

  /**
   * Prepares the options for instantiating the CSV reader.
   *
   * @param array $client_options
   *   An associative array containing configuration options for the
   *   reader. Supported options are:
   *   - delimiter (string, optional, defaults to ','): The delimiter used by
   *     the CSV file being read.
   *   - enclosure (string, optional, defaults to '"'): The enclosure used by
   *     the CSV file being read.
   *   - encoding (string, optional, defaults to 'UTF-8'): The encoding of the
   *     CSV file being read.
   *
   * @return \OpenSpout\Reader\CSV\Options
   *   The options to use for instantiating the reader.
   */
  protected function prepareReaderOptions(array $client_options) {
    $client_options = array_merge(
      $client_options,
      [
        'delimiter' => ',',
        'enclosure' => '"',
        'encoding' => EncodingHelper::ENCODING_UTF8,
      ]
    );

    $options = new Options();

    $options->FIELD_DELIMITER = $client_options['delimiter'];
    $options->FIELD_ENCLOSURE = $client_options['enclosure'];
    $options->ENCODING = $client_options['encoding'];
    // We make sure empty rows are removed. They would always result errors as
    // there's no sense in our case to try importing empty rows.
    $options->SHOULD_PRESERVE_EMPTY_ROWS = FALSE;

    return $options;
  }

  /**
   * Returns the rows contained in the given sheet as objects.
   *
   * The Entity Synchronization framework expects `\stdClass` objects. We
   * collect all rows and convert them to objects.
   *
   * @param \OpenSpout\Reader\SheetInterface $sheet
   *   The sheet.
   *
   * @return object[]
   *   The objects.
   */
  protected function getSheetObjects(SheetInterface $sheet) {
    $objects = [];
    // phpcs:disable
    // @I Support configuring header row if not present in the file
    //    type     : feature
    //    priority : low
    //    labels   : config, format, import
    // phpcs:enable
    $header_values = NULL;

    foreach ($sheet->getRowIterator() as $index => $row) {
      // Indexes start at 1. Get the column names from the header row; they will
      // be used as the remote object property names.
      if ($index === 1) {
        $header_values = $this->getRowValues($row);
        continue;
      }

      $named_cells = array_combine($header_values, $this->getRowValues($row));
      // `array_combine` will return `FALSE` if the number of elements in the
      // arrays do not match. In that case we have a row that the number of
      // columns does not match the expected count and there must be a data
      // error. Continue to the next row.
      if ($named_cells === FALSE) {
        // phpcs:disable
        // @I Log an error for rows with the wrong number of columns
        //    type     : improvement
        //    priority : normal
        //    labels   : import, validation
        // phpcs:enable
        continue;
      }
      $objects[] = $this->arrayToObject($named_cells);
    }

    return $objects;
  }

  /**
   * Returns the values for the given row.
   *
   * @param \OpenSpout\Common\Entity\Row $row
   *   The row.
   *
   * @return array
   *   The values.
   */
  protected function getRowValues(Row $row) {
    return array_map(
      function ($cell) {
        return $cell->getValue();
      },
      $row->getCells()
    );
  }

  /**
   * Converts an associative array to an `\stdClass` object.
   *
   * We don't use `json_encode`/`json_decode` because we can have non UTF-8
   * values, in which case `json_decode` fails.
   *
   * @param array $named_cells
   *   The array to convert.
   *
   * @return object[]
   *   The object.
   */
  protected function arrayToObject(array $named_cells) {
    $object = new \stdClass();
    foreach ($named_cells as $key => $value) {
      $object->{$key} = $value;
    }

    return $object;
  }

}

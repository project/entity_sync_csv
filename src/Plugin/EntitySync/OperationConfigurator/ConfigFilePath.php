<?php

namespace Drupal\entity_sync_csv\Plugin\EntitySync\OperationConfigurator;

use Drupal\entity_sync\Entity\Runner\RunnerInterface;
use Drupal\entity_sync\OperationConfigurator\PluginBase;
use Drupal\entity_sync\OperationConfigurator\SupportsRunnerInterface;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;

use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configurator for CSV imports/exports from a file with on a fixed location.
 *
 * phpcs:disable
 * @EntitySyncOperationConfigurator(
 *   id = "csv_config_file_path",
 *   label = @Translation("CSV file located on a fixed path"),
 *   description = @Translation(
 *     "Use 'CSV file located on a fixed path' types to import CSV rows into Drupal entities from files that are placed on a fixed path by manual action e.g. using an FTP client, or by automation e.g. by an integration."
 *   ),
 *   action_types = {
 *     "import_list",
 *   },
 *   workflow_id = "entity_sync_operation_default",
 * )
 * phpcs:enable
 *
 * Supported additional plugin configuration options:
 * - file_path: (string, required) The full path to the file.
 *
 * @I Validate that the file path exists
 *    type     : bug
 *    priority : normal
 *    labels   : plugin, validation
 * @I Allow defining a different file path per operation (field)
 *    type     : feature
 *    priority : normal
 *    labels   : plugin, operation
 * @I Add field for adding a description to the operation
 *    type     : feature
 *    priority : normal
 *    labels   : plugin, operation
 */
class ConfigFilePath extends PluginBase implements
  ContainerFactoryPluginInterface,
  SupportsRunnerInterface {

  /**
   * The runner for operations of types that use this plugin.
   *
   * @var \Drupal\entity_sync\Entity\Runner\RunnerInterface
   */
  protected $runner;

  /**
   * Constructs a new PluginBase object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\entity_sync\Entity\Runner\RunnerInterface $runner
   *   The runner for file upload configurators.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    RunnerInterface $runner
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->runner = $runner;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_sync_csv.config_file_path_runner')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'remote_resource' => [
        'provider_id' => 'entity_sync_csv',
        'client' => [
          'type' => 'service',
          'service' => 'entity_sync_csv.client',
        ],
      ],
      'operations' => [
        'import_list' => [
          'status' => TRUE,
          'create_entities' => TRUE,
          'update_entities' => TRUE,
          'label' => 'Import from CSV',
        ],
      ],
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(
    array $form,
    FormStateInterface $form_state
  ) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['plugin']['file_path'] = [
      '#type' => 'textfield',
      '#title' => $this->t('File path'),
      '#description' => $this->t(
        'The path where the file will be imported from or exported to.'
      ),
      '#default_value' => $this->configuration['plugin']['file_path'],
      '#required' => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(
    array &$form,
    FormStateInterface $form_state
  ) {
    parent::submitConfigurationForm($form, $form_state);

    $plugin_config = &$this->configuration['plugin'];
    $plugin_config['file_path'] = $form_state->getValue(['plugin', 'file_path']);
  }

  /**
   * {@inheritdoc}
   */
  public function runner() {
    return $this->runner;
  }

}

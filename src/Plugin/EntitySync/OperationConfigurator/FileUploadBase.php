<?php

namespace Drupal\entity_sync_csv\Plugin\EntitySync\OperationConfigurator;

use Drupal\entity_sync\Entity\Runner\RunnerInterface;
use Drupal\entity_sync\OperationConfigurator\PluginBase;
use Drupal\entity_sync\OperationConfigurator\SupportsRunnerInterface;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;

use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Base configurator class for CSV imports/exports via file upload.
 *
 * @I Lazy load the runner service
 *    type     : improvement
 *    priority : normal
 *    labels   : performance
 */
class FileUploadBase extends PluginBase implements
  ContainerFactoryPluginInterface,
  SupportsRunnerInterface {

  /**
   * The runner for operations of types that use this plugin.
   *
   * @var \Drupal\entity_sync\Entity\Runner\RunnerInterface
   */
  protected $runner;

  /**
   * Constructs a new FileUploadBase object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\entity_sync\Entity\Runner\RunnerInterface $runner
   *   The runner for file upload configurators.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    RunnerInterface $runner
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->runner = $runner;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_sync_csv.file_upload_runner')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'remote_resource' => [
        'provider_id' => 'entity_sync_csv',
        'client' => [
          'type' => 'service',
          'service' => 'entity_sync_csv.client',
        ],
      ],
      'operations' => [
        'import_list' => [
          'status' => TRUE,
          'create_entities' => TRUE,
          'update_entities' => TRUE,
          'label' => 'Import from CSV',
        ],
      ],
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function runner() {
    return $this->runner;
  }

}

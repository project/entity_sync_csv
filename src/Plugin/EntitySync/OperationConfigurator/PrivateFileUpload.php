<?php

namespace Drupal\entity_sync_csv\Plugin\EntitySync\OperationConfigurator;

use Drupal\entity_sync\OperationConfigurator\FieldTrait\FileTrait;

/**
 * Configurator for CSV imports/exports via private file upload.
 *
 * phpcs:disable
 * @EntitySyncOperationConfigurator(
 *   id = "csv_private_file_upload",
 *   label = @Translation("CSV file upload (privately stored)"),
 *   description = @Translation(
 *     "Use 'CSV file upload (privately stored)' types to import CSV rows into Drupal entities from files that will be stored in the private file system. This is recommended over publicly stored files for improved security and data privacy."
 *   ),
 *   action_types = {
 *     "import_list",
 *   },
 *   workflow_id = "entity_sync_operation_default",
 * )
 * phpcs:enable
 */
class PrivateFileUpload extends FileUploadBase {

  use FileTrait;

  /**
   * {@inheritdoc}
   */
  public function bundleFieldDefinitions() {
    $fields = parent::bundleFieldDefinitions();
    $fields += $this->privateFileBundleFieldDefinitions();

    return $fields;
  }

}

<?php

namespace Drupal\entity_sync_csv\Plugin\EntitySync\OperationConfigurator;

use Drupal\entity_sync\OperationConfigurator\FieldTrait\FileTrait;

/**
 * Configurator for CSV imports/exports via public file upload.
 *
 * phpcs:disable
 * @EntitySyncOperationConfigurator(
 *   id = "csv_public_file_upload",
 *   label = @Translation("CSV file upload (publicly stored)"),
 *   description = @Translation(
 *     "Use 'CSV file upload (publicly stored)' types to import CSV rows into Drupal entities from files that will be stored in the public file system. In most cases it is recommended to use privately stored files instead for improved security and data privacy."
 *   ),
 *   operations = {
 *     "import_list"
 *   },
 *   workflow_id = "entity_sync_operation_default",
 * )
 * phpcs:enable
 */
class PublicFileUpload extends FileUploadBase {

  use FileTrait;

  /**
   * {@inheritdoc}
   */
  public function bundleFieldDefinitions() {
    $fields = parent::bundleFieldDefinitions();
    $fields += $this->publicFileBundleFieldDefinitions();

    return $fields;
  }

}

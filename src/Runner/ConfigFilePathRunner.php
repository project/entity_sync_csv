<?php

namespace Drupal\entity_sync_csv\Runner;

use Drupal\entity_sync\Entity\OperationInterface;
use Drupal\entity_sync\OperationConfigurator\PluginInterface;

/**
 * Runner for CSV configuration file path configurator plugins.
 */
class ConfigFilePathRunner extends FileRunnerBase {

  /**
   * {@inheritdoc}
   */
  protected function getFilePath(
    OperationInterface $operation,
    PluginInterface $plugin
  ) {
    return $plugin->getConfiguration()['plugin']['file_path'];
  }

}

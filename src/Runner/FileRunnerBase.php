<?php

namespace Drupal\entity_sync_csv\Runner;

use Drupal\entity_sync\Entity\OperationInterface;
use Drupal\entity_sync\Exception\InvalidConfigurationException;
use Drupal\entity_sync\MachineName\Field\Operation as OperationField;
use Drupal\entity_sync\Import\ManagerInterface as ImportManagerInterface;
use Drupal\entity_sync\Entity\Runner\RunnerInterface;
use Drupal\entity_sync\OperationConfigurator\PluginInterface;

use Drupal\Core\Entity\EntityTypeManagerInterface;

use Psr\Log\LoggerInterface;

/**
 * Base class for CSV file runners.
 *
 * @I Lazy load import/export managers
 *    type     : improvement
 *    priority : low
 *    labels   : performance
 */
abstract class FileRunnerBase implements RunnerInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The Entity Synchronization entity import manager.
   *
   * @var \Drupal\entity_sync\Import\ManagerInterface
   */
  protected $importManager;

  /**
   * The module's logger channel.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * Returns the file path for the given operation and configurator plugin.
   *
   * @param \Drupal\entity_sync\Entity\OperationInterface $operation
   *   The operation entity.
   * @param \Drupal\entity_sync\OperationConfigurator\PluginInterface $plugin
   *   The configurator plugin of the operation.
   */
  abstract protected function getFilePath(
    OperationInterface $operation,
    PluginInterface $plugin
  );

  /**
   * Constructs a new FileRunnerBase object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\entity_sync\Import\ManagerInterface $import_manager
   *   The Entity Synchronization entity import manager.
   * @param \Psr\Log\LoggerInterface $logger
   *   The module's logger channel.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    ImportManagerInterface $import_manager,
    LoggerInterface $logger
  ) {
    $this->entityTypeManager = $entity_type_manager;
    $this->importManager = $import_manager;
    $this->logger = $logger;
  }

  /**
   * {@inheritdoc}
   */
  public function run(OperationInterface $operation, array $context = []) {
    $plugin = $this->entityTypeManager
      ->getStorage('entity_sync_operation_type')
      ->loadWithPluginInstantiated($operation->bundle())
      ->getPlugin();
    $plugin_configuration = $plugin->getConfiguration();
    if (!isset($plugin_configuration['action_type'])) {
      throw new InvalidConfigurationException(sprintf(
        'The action type is not defined for the plugin with ID "%s".',
        $plugin->getPluginId()
      ));
    }

    if ($plugin_configuration['action_type'] !== 'import_list') {
      throw new InvalidConfigurationException(sprintf(
        'The Entity Synchronization CSV file runner currently supports the `import_list` action type only.'
      ));
    }

    $this->import($operation, $plugin);
  }

  /**
   * Executes the import for the given operation.
   *
   * @param \Drupal\entity_sync\Entity\OperationInterface $operation
   *   The operation entity.
   * @param \Drupal\entity_sync\OperationConfigurator\PluginInterface $plugin
   *   The configurator plugin of the operation.
   */
  protected function import(
    OperationInterface $operation,
    PluginInterface $plugin
  ) {
    $client_options = [
      'file_path' => $this->getFilePath($operation, $plugin),
    ];
    $context = [
      'options' => ['client' => $client_options],
    ];

    $transition_id = 'complete';
    try {
      $this->importManager->importRemoteList(
        $operation->bundle(),
        [],
        [
          'client' => $client_options,
          'context' => $context,
        ]
      );
    }
    catch (\Throwable $throwable) {
      $transition_id = 'fail';
      $this->logger->error(
        'The operation with ID "@operation_id" has partly or fully failed: @throwable @message',
        [
          '@operation_id' => $operation->id(),
          '@throwable' => get_class($throwable),
          '@message' => $throwable->getMessage(),
        ]
      );
    }

    $operation
      ->get(OperationField::STATE)
      ->first()
      ->applyTransitionById($transition_id);
    $this->entityTypeManager
      ->getStorage('entity_sync_operation')
      ->save($operation);
  }

}

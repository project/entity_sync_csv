<?php

namespace Drupal\entity_sync_csv\Runner;

use Drupal\entity_sync_csv\Plugin\EntitySync\OperationConfigurator\PrivateFileUpload;
use Drupal\entity_sync_csv\Plugin\EntitySync\OperationConfigurator\PublicFileUpload;

use Drupal\entity_sync\Entity\OperationInterface;
use Drupal\entity_sync\Import\ManagerInterface as ImportManagerInterface;
use Drupal\entity_sync\OperationConfigurator\PluginInterface;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\File\FileSystemInterface;

use Psr\Log\LoggerInterface;

/**
 * Runner for CSV file upload configurator plugins.
 */
class FileUploadRunner extends FileRunnerBase {

  /**
   * The Drupal file system.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * Constructs a new FileUploadRunner object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\entity_sync\Import\ManagerInterface $import_manager
   *   The Entity Synchronization entity import manager.
   * @param \Psr\Log\LoggerInterface $logger
   *   The module's logger channel.
   * @param \Drupal\Core\File\FileSystemInterface $file_system
   *   The Drupal file system.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    ImportManagerInterface $import_manager,
    LoggerInterface $logger,
    FileSystemInterface $file_system
  ) {
    parent::__construct(
      $entity_type_manager,
      $import_manager,
      $logger
    );

    $this->fileSystem = $file_system;
  }

  /**
   * {@inheritdoc}
   */
  protected function getFilePath(
    OperationInterface $operation,
    PluginInterface $plugin
  ) {
    $field_name = NULL;
    if ($plugin instanceof PrivateFileUpload) {
      $field_name = 'private_file';
    }
    if ($plugin instanceof PublicFileUpload) {
      $field_name = 'public_file';
    }

    if (!$field_name) {
      throw new \RuntimeException(
        'The Entity Synchronization CSV upload runner can currently only be used by the `csv_private_file_upload` and `csv_public_file_upload` configurator plugins, or plugins that extend them.'
      );
    }

    return $this->fileSystem->realpath(
      $operation->get($field_name)->first()->entity->getFileUri()
    );
  }

}
